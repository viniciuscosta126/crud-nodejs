const path = require("path");

//Apenas nome do arquivo atual
console.log(path.basename(__filename));

//Nome diretorio atual
console.log(path.dirname(__filename));

//Extensao do Arquivo

console.log(path.extname(__filename));

//Criar objeto path
console.log(path.parse(__filename));

//Juntar caminhos de arquivos
console.log(path.join(__dirname,'test','teste.html'))
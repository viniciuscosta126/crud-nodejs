const mongoose = require("mongoose");

const connectToDataBase = async () => {
  await mongoose.connect(
    `mongodb://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@nodejs-shard-00-00.kt4jv.mongodb.net:27017,nodejs-shard-00-01.kt4jv.mongodb.net:27017,nodejs-shard-00-02.kt4jv.mongodb.net:27017/?ssl=true&replicaSet=atlas-9de8ez-shard-0&authSource=admin&retryWrites=true&w=majority`,
    (error) => {
      if (error) {
        return console.log("Ocorreu um erro: ", error);
      }
      return console.log("Conexao realizada com sucesso");
    }

  );
};
module.exports = connectToDataBase;

